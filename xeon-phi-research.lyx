#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass beamer
\begin_preamble
\usepackage{lmodern}
\usetheme{metropolis}
%\usepackage{pgfpages}
%\setbeameroption{show notes on second screen=bottom} % 
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_title "Intel® Xeon® Phi Knights Corner"
\pdf_author "Antognazza Francesco - mat. 899679"
\pdf_subject "First generation of Intel® coprocessor"
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Intel® Xeon® Phi Knights Corner
\end_layout

\begin_layout Subtitle
First generation of Intel® coprocessor
\end_layout

\begin_layout Author
Antognazza Francesco - mat.
 899679
\end_layout

\begin_layout Institute
Politecnico di Milano
\end_layout

\begin_layout TitleGraphic
\begin_inset Graphics
	filename img/polimi.svg
	scale 30

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Contents
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\end_deeper
\begin_layout Section
Architecture
\end_layout

\begin_layout Frame
\begin_inset Argument 2
status open

\begin_layout Plain Layout

+(1)- | alert@+(1)
\end_layout

\end_inset


\begin_inset Argument 4
status open

\begin_layout Plain Layout
Product overview
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
many core platform: 62 cores@1.1GHz, 244 threads
\end_layout

\begin_layout Itemize
dual issue scalar unit, one-per-clock throughput
\end_layout

\begin_layout Itemize
over 1 TeraFlops/s with double precision
\end_layout

\begin_layout Itemize
high performance/watt (up to 4x w.r.t.
 Xeon E5 family)
\end_layout

\begin_layout Itemize
independent from the host, based upon Linux kernel 2.6.34
\end_layout

\begin_layout Itemize
communications over PCI Express 2.0
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Note Comment
status open

\begin_layout Plain Layout
61 cores available, 4 thread per core (round robin); bi-directional core
 interconnection ring bus; distributed tag directory
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 2
status open

\begin_layout Plain Layout

+(1)- | alert@+(1)
\end_layout

\end_inset


\begin_inset Argument 4
status open

\begin_layout Plain Layout
Vector Processing Engine
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
512-bit SIMD instructions (IMCI)
\end_layout

\begin_deeper
\begin_layout Itemize
8x 64-bit double precision ops
\end_layout

\begin_layout Itemize
16x 32-bit single precision ops
\end_layout

\end_deeper
\begin_layout Itemize
32 GP Vector Registers
\end_layout

\begin_layout Itemize
8x 16-bit mask registers
\end_layout

\begin_layout Itemize
4 clock latency for most instructions
\end_layout

\begin_layout Itemize
IEEE 754 standard compliance
\end_layout

\begin_layout Itemize
16 SP and 8 DP ALUs
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Memory
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Float table
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="6" columns="3">
<features tabularvalignment="middle">
<column alignment="center" valignment="top" width="3cm">
<column alignment="center" valignment="top" width="3cm">
<column alignment="center" valignment="top" width="3cm">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
L1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
L2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Memory
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
32KB I-cache + 32KB D-cache
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
512KB cache
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
8 GB GDDR5
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
8 way associative
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
8 way associative
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
8 memory controller
\end_layout

\begin_layout Plain Layout
16 GDDR5 channels
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3 cycle access
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
11 cycle best access
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
300ns access time
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
fully coherent
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
fully coherent
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
ECC
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
32 outstanding requests
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
352 GB/s bandwidth
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Note Comment
status open

\begin_layout Plain Layout
MEMORY:
\end_layout

\begin_layout Plain Layout
~7GB available, 1GB accessible as buffer
\end_layout

\begin_layout Plain Layout
2.2x Xeon E5 bandwidth
\end_layout

\end_inset


\end_layout

\begin_layout Section
Compiler
\end_layout

\begin_layout Frame
\begin_inset Argument 2
status open

\begin_layout Plain Layout

+(1)- | alert@+(1)
\end_layout

\end_inset


\begin_inset Argument 4
status open

\begin_layout Plain Layout
Heterogeneous Compiler
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
supports C/C++ with OpenMP pragmas, Cilk Plus, Fortran (with some limitations)
\end_layout

\begin_layout Itemize
generates two binary for both architectures
\end_layout

\begin_layout Itemize
automatically transfer program to MIC
\end_layout

\begin_layout Itemize
explicit and implicit data transfer through DMA
\end_layout

\begin_layout Itemize
a small section of memory maintained at same virtual address
\end_layout

\begin_layout Itemize
can generate only native MIC code
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Note Comment
status open

\begin_layout Plain Layout
2: computation can be done by host even if coprocessor is not available
 at execution time
\end_layout

\begin_layout Plain Layout
4: through pragmas
\end_layout

\begin_layout Plain Layout
5: useful for easily porting old code
\end_layout

\begin_layout Plain Layout
6: for some particular optimization purposes
\end_layout

\end_inset


\end_layout

\begin_layout Section
Communication
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
TCP/IP - sockets
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Float table
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="4" columns="4">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Card
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Network
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Device IP
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Host IP
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
mic0
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
172.31.1.1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
172.31.1.254
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
mic1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
172.31.2.1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
172.31.2.254
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
mic2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
172.31.3.1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
172.31.3.254
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
default network configurations
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Sockets uses the normal implementation conventions, standards and APIs.
\end_layout

\begin_layout Standard
Allowed communication:
\end_layout

\begin_layout Itemize
Host to/from MIC
\end_layout

\begin_layout Itemize
MIC to/from other MICs
\end_layout

\begin_layout Itemize
External to/from MIC
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Note Comment
status open

\begin_layout Plain Layout
2: locally
\end_layout

\begin_layout Plain Layout
3: including coprocessors under different hosts
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
File and console I/O
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Same as for standard Linux, usable from offload code
\end_layout

\begin_layout Itemize
console proxy
\end_layout

\begin_layout Itemize
configurable NFS share 
\end_layout

\begin_layout Itemize
files persist after application ends
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Note Comment
status open

\begin_layout Plain Layout
1: stderr and stdout redirected to host's one
\end_layout

\begin_layout Plain Layout
2: also can be mounted as root FS
\end_layout

\begin_layout Plain Layout
3: reliability and security issues, but easy to use
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Data transfers
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Supported explicit and implicit transfers for variables and offloaded code
\end_layout

\begin_layout Itemize
large arrays transferred by DMA
\end_layout

\begin_layout Itemize
non-pointers transferred by 
\emph on
coi_misc_buffer
\end_layout

\begin_layout Itemize
transfers of pages of 4K at aligned addresses give best performance
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Note Comment
status open

\begin_layout Plain Layout
explicit: offload compiler pragmas
\end_layout

\begin_layout Plain Layout
implicit: automatically inferred by compiler
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Comment
status open

\begin_layout Plain Layout
1: can save clock cycles doing other computations
\end_layout

\begin_layout Plain Layout
2: DMA setup / data marshaling time too costly
\end_layout

\begin_layout Plain Layout
3: no bitmask register needed
\end_layout

\end_inset


\end_layout

\begin_layout Section
Performance and optimization
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Importance of optimization
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Only
\begin_inset Argument 1
status open

\begin_layout Plain Layout

1
\end_layout

\end_inset


\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename img/performance-MIC-xeon-2.png
	lyxscale 45
	scale 45

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Intel® Xeon® processor E5-2670 vs.
 Intel® Xeon Phi® coprocessor 5110P & SE10P/X
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Pause

\end_layout

\begin_layout Only
\begin_inset Argument 1
status open

\begin_layout Plain Layout

2
\end_layout

\end_inset


\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename img/case-studies-3.png
	lyxscale 45
	scale 45

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Tachyon ray tracer benchmark
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Pause

\end_layout

\begin_layout Only
\begin_inset Argument 1
status open

\begin_layout Plain Layout

3
\end_layout

\end_inset


\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename img/optimization-process.png
	lyxscale 50
	scale 50

\end_inset


\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 2
status open

\begin_layout Plain Layout

+(1)- | alert@+(1)
\end_layout

\end_inset


\begin_inset Argument 4
status open

\begin_layout Plain Layout
Algorithm
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
choose SIMD-friendly algorithms
\end_layout

\begin_layout Itemize
balance performance and accuracy
\end_layout

\begin_layout Itemize
choose a balanced work between threads
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Note Comment
status open

\begin_layout Plain Layout
use approximated functions e.g.
 sqrt function over X
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 2
status open

\begin_layout Plain Layout

+(1)- | alert@+(1)
\end_layout

\end_inset


\begin_inset Argument 4
status open

\begin_layout Plain Layout
Threading
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
redistribute work of large trip counts loops to different threads
\end_layout

\begin_layout Itemize
loops with large amounts of code are also good candidates
\end_layout

\begin_layout Itemize
check for serialization
\end_layout

\begin_layout Itemize
exploit thread affinity
\end_layout

\begin_layout Itemize
avoid core 
\begin_inset Quotes eld
\end_inset

overcrowding
\begin_inset Quotes erd
\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Note Comment
status open

\begin_layout Plain Layout
3: e.g locks
\end_layout

\begin_layout Plain Layout
4: e.g.
 OpenMP scatter, which maintains as much locality as possible distributing
 threads but keeping adjacent thread numbers next to each other.
 Use scatter when #threads <= #available cores
\end_layout

\begin_layout Plain Layout
5: good reasons to use fewer: minimize cache/TLB thrashing when too many
 threads compete for L1 or L2; lower competition for the VPE; minimize requests
 to main memory.
 >= 2 threads/core gives >90% of instruction issue bandwidth
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 2
status open

\begin_layout Plain Layout

+(1)- | alert@+(1)
\end_layout

\end_inset


\begin_inset Argument 4
status open

\begin_layout Plain Layout
Vectorization
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Loops with large trip counts are good candidates for data-parallelism using
 SIMD vectorization.
\end_layout

\begin_layout Standard

\series bold
Avoid
\series default
:
\end_layout

\begin_layout Itemize
inner loops with constructors, calling functions, conditionals
\end_layout

\begin_layout Itemize
unintended conversions
\end_layout

\begin_layout Itemize
unintended data dependencies
\end_layout

\begin_layout Standard

\series bold
Exploit
\series default
:
\end_layout

\begin_layout Itemize
data locality
\end_layout

\begin_layout Itemize
compiler hints
\end_layout

\begin_layout Itemize
smallest usable vector data type
\end_layout

\begin_layout Itemize
Cilk language
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Note Comment
status open

\begin_layout Plain Layout
5: Cilk Plus and OpenMP pragmas
\end_layout

\begin_layout Plain Layout
6: FP over DP
\end_layout

\begin_layout Plain Layout
7: deprecated
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 2
status open

\begin_layout Plain Layout

+(1)- | alert@+(1)
\end_layout

\end_inset


\begin_inset Argument 4
status open

\begin_layout Plain Layout
Memory
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
force memory alignment for high-speed vector processing
\end_layout

\begin_deeper
\begin_layout Itemize

\emph on
__attribute__((aligned(n))
\emph default
 for static memory
\end_layout

\begin_layout Itemize

\emph on
__mm_aligned_malloc()
\emph default
 for dynamic memory
\end_layout

\end_deeper
\begin_layout Itemize
compiler hints for data locality
\end_layout

\begin_layout Itemize
minimize gathers and scatters
\end_layout

\begin_layout Itemize
cache prefetching
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Note Comment
status open

\begin_layout Plain Layout
3: by converting arrays of structures (AOS) to structures of arrays (SOA)
\end_layout

\begin_layout Plain Layout
4: looking at L1 and L2 miss rate; 1000 cycles in advance for L2, 50 cycles
 for L1
\end_layout

\end_inset


\end_layout

\begin_layout Section
\start_of_appendix
Appendix
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
References
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-1"
literal "false"

\end_inset

https://software.intel.com/en-us/xeon-phi/mic/training
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-7"

\end_inset

https://emit.tech/wp-content/uploads/2015/10/Intel_02_CaseStudies.pdf
\end_layout

\end_deeper
\end_body
\end_document
