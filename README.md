![build status](https://gitlab.com/fantognazza/xeon-phi-research/badges/master/build.svg)

# Abstract

Presentation on Intel Xeon Phi Knight's Corner coprocessor developed during the Advanced Computer Architectures course.

The presentation uses information mainly from Intel pubblic available documentation.

# Files description

* **xeon-phi-research.lyx**: source file of document to be used with Lyx editor

* **xeon-phi-research.tex**: LaTeX source exported from Lyx editor. It's used by gitlab-ci to generate PDFs

* **img**: contains some graphic sources

# Download the PDF

[xeon-phi-research.pdf](https://gitlab.com/fantognazza/xeon-phi-research/-/jobs/artifacts/master/raw/xeon-phi-research.pdf?job=compile_pdf)
